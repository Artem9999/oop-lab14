﻿namespace WindowsFormsApp4
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.OpenFileDialog = new System.Windows.Forms.Button();
            this.FileListBox = new System.Windows.Forms.ListBox();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.Cript = new System.Windows.Forms.ToolStripMenuItem();
            this.Decrypt = new System.Windows.Forms.ToolStripMenuItem();
            this.Zip = new System.Windows.Forms.ToolStripMenuItem();
            this.UnZip = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // OpenFileDialog
            // 
            this.OpenFileDialog.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.OpenFileDialog.Location = new System.Drawing.Point(12, 159);
            this.OpenFileDialog.Name = "OpenFileDialog";
            this.OpenFileDialog.Size = new System.Drawing.Size(136, 36);
            this.OpenFileDialog.TabIndex = 0;
            this.OpenFileDialog.Text = "Вибрати файл";
            this.OpenFileDialog.UseVisualStyleBackColor = true;
            this.OpenFileDialog.Click += new System.EventHandler(this.OpenFileDialog_Click);
            // 
            // FileListBox
            // 
            this.FileListBox.AllowDrop = true;
            this.FileListBox.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FileListBox.FormattingEnabled = true;
            this.FileListBox.ItemHeight = 23;
            this.FileListBox.Location = new System.Drawing.Point(12, 213);
            this.FileListBox.Name = "FileListBox";
            this.FileListBox.Size = new System.Drawing.Size(776, 211);
            this.FileListBox.TabIndex = 1;
            this.FileListBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.listbox1_DragDrop);
            this.FileListBox.DragEnter += new System.Windows.Forms.DragEventHandler(this.listbox1_DragEnter);
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Cript,
            this.Decrypt,
            this.Zip,
            this.UnZip});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(800, 31);
            this.menuStrip.TabIndex = 2;
            this.menuStrip.Text = "menuStrip1";
            // 
            // Cript
            // 
            this.Cript.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Cript.Name = "Cript";
            this.Cript.Size = new System.Drawing.Size(124, 27);
            this.Cript.Text = "Зашифрувати";
            this.Cript.Click += new System.EventHandler(this.Cript_Click);
            // 
            // Decrypt
            // 
            this.Decrypt.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Decrypt.Name = "Decrypt";
            this.Decrypt.Size = new System.Drawing.Size(132, 27);
            this.Decrypt.Text = "Розшифрувати";
            this.Decrypt.Click += new System.EventHandler(this.Decrypt_Click);
            // 
            // Zip
            // 
            this.Zip.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Zip.Name = "Zip";
            this.Zip.Size = new System.Drawing.Size(100, 27);
            this.Zip.Text = "Архівувати";
            this.Zip.Click += new System.EventHandler(this.Zip_Click);
            // 
            // UnZip
            // 
            this.UnZip.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.UnZip.Name = "UnZip";
            this.UnZip.Size = new System.Drawing.Size(125, 27);
            this.UnZip.Text = "Розархівувати";
            this.UnZip.Click += new System.EventHandler(this.UnZip_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::WindowsFormsApp4.Properties.Resources.Backgrounds_Wallpaper_with_ornament_035516_;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.FileListBox);
            this.Controls.Add(this.OpenFileDialog);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Name = "Form1";
            this.Text = "Програма редактування файлів";
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button OpenFileDialog;
        private System.Windows.Forms.ListBox FileListBox;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem Cript;
        private System.Windows.Forms.ToolStripMenuItem Decrypt;
        private System.Windows.Forms.ToolStripMenuItem Zip;
        private System.Windows.Forms.ToolStripMenuItem UnZip;
    }
}

