﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using ClassLibrary1;
namespace WindowsFormsApp4
{


        public partial class Form1 : Form
    {
        OpenFileDialog OPF = new OpenFileDialog();
        ArrayList list = new ArrayList();
        public Form1()
        {
            InitializeComponent();
            
        }
        private void listbox1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false) == true)
            {
                e.Effect = DragDropEffects.All;
            }
        }
        private void listbox1_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

            foreach (string file in files)
            {
                FileListBox.Items.Add(file);
                list.Add(file);
            }
        }
        private void OpenFileDialog_Click(object sender, EventArgs e)
        {      
           
            if (OPF.ShowDialog() == DialogResult.OK)
            {
                list.Add(OPF.FileName);

                string file = OPF.FileName;
                FileListBox.Items.Add(file);
                
            }
        }

        private void Cript_Click(object sender, EventArgs e)
        {
            string inputFile=null;
            foreach (string o in list) 
            {
                inputFile = o;
            string outputFile=inputFile + ".crypt";
            CryptoHelper.EncryptFile(inputFile, outputFile);
            }

        }

        private void Decrypt_Click(object sender, EventArgs e)
        {
            string inputFile = null;
            string type=null;
            string type2 = null;
            foreach (string o in list)
            {
                inputFile = o;
                
                string answer=inputFile;
                string pattern =@"\w*(\.\w*)(\.crypt)";
                Regex regex = new Regex(pattern);
                Match match = regex.Match(answer);
                if (match.Success)
                {
                   type= match.Groups[1].Value;
                   type2 = match.Groups[2].Value;
                }
                if (type2 == ".crypt")
                {
                    string outputFile = inputFile + type;
                    CryptoHelper.DecryptFile(inputFile, outputFile);
                }
            }
        }

        private void Zip_Click(object sender, EventArgs e)
        {
            string inputFile = null;
            foreach (string o in list)
            {
                inputFile = o;
                string outputFile = inputFile + ".gzar";
                Zipfiles.Compress(inputFile, outputFile);
            }
        }

        private void UnZip_Click(object sender, EventArgs e)
        {
            string inputFile = null;
            string type = null;
            string type2 = null;
            foreach (string o in list)
            {
                inputFile = o;
                string answer = inputFile;
                string pattern = @"\w*(\.\w*)(\.gzar)";
                Regex regex = new Regex(pattern);
                Match match = regex.Match(answer);
                if (match.Success)
                {
                    type = match.Groups[1].Value;
                    type2 = match.Groups[2].Value;
                }
                if (type2 == ".gzar")
                {
                    string outputFile = inputFile + type;
                    Zipfiles.Decompress(inputFile, outputFile);
                }
            }
        }
    }
}
